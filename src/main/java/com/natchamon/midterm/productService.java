/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.natchamon.midterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class ProductService {
    private static ArrayList<Product> productList = new ArrayList<>();;
    static{
        load();
        //Mock Data
//        userList.add(new User("admin", "password"));
//        userList.add(new User("user1", "password"));
//        userList.add(new User("user2", "password"));
    }
 public static boolean addProduct(Product product){
        productList.add(product);
        save();
        return true;
    }
    // Delete (D)
    public static boolean delProduct(Product product){
        productList.remove(product);
        save();
        return true;
    }
    public static boolean delProduct(int index){
        productList.remove(index);
        save();
        return true;
    }
    //read (R)
    public static ArrayList<Product> getProduct(){
        save();
        return productList;
    }  
    public static Product getProduct(int index){
        save();
        return productList.get(index);
    }
    // Update (U)
    public static boolean updateProduct(int index, Product data){
        productList.set(index, data);
        save();
        return true;
    }
    
    public static boolean clearProduct() {
        productList.clear();
        save();
        return true;
    }
    public static int showTotalAmount() {
        int amount = 0;
        for (int i = 0; i < productList.size(); i++) {
            amount += productList.get(i).getAmount();
        }
        return amount;
    }

    public static double showTotalPrice() {
        double price = 0;
        for (int i = 0; i < productList.size(); i++) {
            price += productList.get(i).getPrice() * productList.get(i).getAmount();
 ;
        }
        return price;
    }
    // file
   public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Falong.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Falong.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }
   
}